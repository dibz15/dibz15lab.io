#!/bin/bash



if [ $# -eq 0 ]
  then
    echo "Usage: mp4togif.sh <inputFile> <outputFile>"
fi


ffmpeg -ss 30 -t 3 -i input.mp4 -vf "fps=10,scale=320:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 output.gif
