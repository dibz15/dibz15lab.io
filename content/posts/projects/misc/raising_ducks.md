---
post_title: Raising Ducks Amidst Covid-19
hero: /images/posts/projects/duck_coop/duck_pet_opt.gif
excerpt_desc: My experiences raising ducks during the coronavirus shutdown.
unlisted_post: false
date: 2020-03-31

id: "raising_ducks_covid"
---

In the midst of the national and international Covid-19 crisis, my wife and I decided to escape to rural Oregon and stay with my parents. Me being recently graduated and her being a teacher unable to return to school, it seemed like a good time to spend time with my family and also support them in this time. I'm hoping to find a way that I can sustainably do remote work until all this is over, and help support the people I know through this time.

<h3 class="underline--magical"> Why Ducks? </h3>

Until we can see what the future holds for all of us, our family thought about finding work around the farm to do that can keep us busy and also provide (eventually) some farm-fresh foods. We definitely wanted to start a vegetable garden, but also came up with an idea to get fresh eggs. Well, as it turns out, ducks are easier to care for and often produce more eggs than chickens. They're also less vulnerable to temperature extremes, which the Oregon desert is prone to at different times of the year. I've also always wanted a pet duck, so there's that.


<h4 class="underline--magical"> Day 0 --- Building a DIY Duck Tractor </h4>

For those unfamiliar with the term (as I was), tractor in this sense is actually a type of enclosure that you can move around. A duck tractor is a nice enclosed area that can be moved around so the ducks can graze in different areas on different days without destroying your grass. They're messy creatures, I guess. 

As it turns out, this is something that people generally build themselves, so we thought that with some extra 1" PVC pipe that my dad has laying around, we could construct our own. The total dimensions are 5'x6'x4' (WxLxH) and below are some build pictures. The chicken wire that we got is 4'10" tall, so that restricted the build dimesnions a bit.


<!-- ![Supplies](/images/posts/projects/duck_coop/coop_supplies.jpg)
<div class="image-caption">Materials.</div> -

![Wire](/images/posts/projects/duck_coop/coop_wire.jpg)
<div class="image-caption">Chicken wire from nearby feed store.</div>

-->

![Frames](/images/posts/projects/duck_coop/coop_frames.jpg)
<div class="image-caption">PVC frames that we constructed. These are 4'x5'.</div>

After assembling the entire PVC frame, we went over every joint and glued it together with PVC glue. In the end, it was pretty sturdy even if the frame was a bit bent. 

![Frames_built](/images/posts/projects/duck_coop/coop_frame_assembled.jpg)
<div class="image-caption">Fully constructed frame with chicken wire wrapping.</div>

After assembling the complete frame, we began wrapping it in chicken wire. The chicken wire was carefully wrapped around the pipes and held in place with lots of zip-ties. 

TBC.

<h4 class="underline--magical"> Day 1 --- Getting Started </h4>

![Duck_head_shake](/images/posts/projects/duck_coop/ducks_box_opt.gif)
<div class="image-caption">Coming home.</div>


Even though it was all a bit impulsive, we went to a nearby feed store that gets ducklings at this time of year and picked up three ducks as well as everything we would need to start caring for them. According to the store, these are American Pekin ducks. In case anyone's interested, here's what we picked up:

- Un-medicated chick starter feed with 20% crude protein.
- Pine shavings for bedding.
- Feed and water dishes.
- Heat lamp with a 250W bulb.

Additionally, we used a Rubbermaid storage tote that we had at home to house them until they grow large enough to live in a duck house. 
After we brought them home, we set up their home in the garage with the heat lamp, and let them settle in. With a lot of deliberation, we also came up with their names. I've been studying Korean, so for mine we came up with 오리 (Ori), my wife's is 뚝뚝 (Tuk-tuk), and my sister's is Stropi. My sister wanted something different, so hers is in Romanian. 

![Duck_tote](/images/posts/projects/duck_coop/duck_tote.jpg)
<div class="image-caption">A small tote serves as a good temporary home.</div>

Also, it's worth mentioning that although we would like to get some eggs out of our ducks eventually, it's entirely possible that they're all males. When you buy out of a 'straight run' at the store, no one has sexed them and there's really no way to tell what they are. It's just the luck of the draw.


[//]: # (Writing about duck house scavenger hunt)
<h4 class="underline--magical"> Day 2 --- Duck House Scavenger Hunt </h4>

![Duck_head_shake](/images/posts/projects/duck_coop/duck_shake_opt.gif)
<div class="image-caption">This is how I felt after looking for a duck 'coop' for two days.</div>

Unbeknownst to us, it's almost impossible to find a proper duck house, even in places they sell ducks. Chicken coop? Sure. Dog house? Got it. But if you want to house your ducks, it's time to look far and wide or build your own. According to our Internet searches, ducks need a few things in a nice outdoor home. Keep in mind this is just where we will keep them at night.

- Sturdy walls that can't be breached by raccoons, foxes, or coyotes.
- Enough space that each duck has at least 4 sq. ft. as well as enough space for their food and water.
- A floor that is easy to clean out each day, and is close to the ground. Ducks apparently don't like ramps.

This doesn't sound like too much to ask, but I guess it is. We visited the 5 feed stores in our area, along with a pet store and Home Depot. Overall, it took us two whole days to find one that we thought would work, but we had to order it online through Home Depot. [This](https://www.homedepot.com/p/TRIXIE-Chicken-Coop-55963/204676041) is the exact model. 

![Duck-house](/images/posts/projects/duck_coop/duck_house.jpg)
<div class="image-caption">The promised ducky home. Beautiful, isn't it?</div>

If our ducks refuse to go up the small ramp, we may end up cutting off the legs of the house. Even so, it's as close as we could find to what we want without breaking the bank, and I also didn't have to build it from scratch. 

Perhaps we could have found something better or cheaper if we had looked more, but with the ducks growing at an inconceivable pace we were concerned at how quickly they would outgrow their current tote home.

<h4 class="underline--magical"> Day 3 & 4 --- Little Personalities </h4>

![Ducklings-sleep](/images/posts/projects/duck_coop/ducks_sleep_opt.gif)
<div class="image-caption">It really is about the cuddles.</div>

Even though I've wanted a pet duck for a long time, I always figured that the cons would outweigh the pros of having them. I was absolutely wrong. We're probably still in the honeymoon stage of duck ownership, but after having the ducks for several days their individual personalities have really had a chance to shine through. 

For example, Ori likes to bite, Tuk-tuk loves to sleep on Alicia's lap, and if Stropi is left alone for even a second, he starts screaming and crying as if he were the last duck left on the planet (here I'm assuming 'he', but we actually have no idea of their sexes).

A couple other things that we've learned during this time:

- Ducklings eat and drink **a lot**.
- Ducklings poop **a lot**.
- Ducklings grow *really, really fast*.
- Ducklings will attempt to put *anything* in their mouths.

To keep up with their rapid metabolisms, we have to replace their food and water twice a day, and clean out the litter from their tote every morning and night. 
And on that last note, (as of day 4) the ducklings are twice the size they were when we bought them. On the first day they could fit in one hand, and now they fit into two. 

![Duckling-chasing-string](/images/posts/projects/duck_coop/duck_string_opt.gif)
<div class="image-caption">Ducklings love anything worm shaped.</div>

Another thing that we have found is that they really like to have floor time. As of day 4, it's been too cold to let them have some foraging time outside, but we have been letting them roam our living room on some towels we have laid down. The ducks love running around in circles and finding things to try and put in their mouths. 

Hopefully soon the weather will be good enough to let them have some outdoor time where they can peck at everything to their hearts' content.

![Duckling-together](/images/posts/projects/duck_coop/ducks_together.jpg)
<div class="image-caption">They love to be close together.</div>


<h4 class="underline--magical"> Day 5 --- The Guinea Pig </h4>

As we take care of them each day, we are finding that each duck's personality is becoming more and more distinct. The primary example of this so far is Alicia's duck, who now loves to sleep on her lap and is content to remain there for a long time. Alicia also introduced Tuk-tuk to her pet guinea pig, Zana, and we found that they get along really well. Well, besides Tuk-tuk trying to eat Zana's ear.

![Zana-snuggle](/images/posts/projects/duck_coop/zana_snuggle.jpg)
<div class="image-caption">Yep.</div>

![Zana-snuggle-2](/images/posts/projects/duck_coop/zana_snuggle2.jpg)
<div class="image-caption">Tuk-tuk loves to sleep.</div>

The question for us is, does Zana actually like Tuk-tuk or is she just indifferent? My guess is the latter, but it's cute anyway.

<h4 class="underline--magical"> Day 7 --- Bath Day </h4>

In the first few weeks of a domestic duck's life, it's important that any water time they have is well-supervised since they don't have any oils in their feathers to keep them from sinking. Since we wanted to clean them up and give them a chance to swim safely, we thought it would be fun to let them swim around in the sink.

![Duck-Bath](/images/posts/projects/duck_coop/duck_bath.gif)
<div class="image-caption">Swimming around.</div>

Eventually they all figured out how to dive around and swim under the water, too! Hopefully someday soon they will be able to swim outside, too.

<h4 class="underline--magical"> Day 8 --- First Forage </h4>

![Duckling-Walk](/images/posts/projects/duck_coop/duck_walk.jpg)
<div class="image-caption">They're growing <i>really</i> fast.</div>

One thing that we'll need our ducks to do is forage in the grass without supervision when they're full grown. Ducks also need to be able to forage for 'grit' (dirt, basically) to help them digest their food, so getting them some outside time at this age is a good iea. 

Another fun thing that we discovered is that they like to follow Alicia around as she walks! She walked them around the house a few times.

![Duckling-Follow](/images/posts/projects/duck_coop/duck_follow.gif)
<div class="image-caption">The ducklings going round and round.</div>


<h4 class="underline--magical"> Day 10 --- New Duck House </h4>

As we were waiting for the duck house from Home Depot, we were growing concerned that it wouldn't arrive before our ducks outgrew their box. In fact, they're already started to develop their adult feathers that are fully white; rather than the soft downy yellow feathers they've had so far.

![Duckling-Feathers](/images/posts/projects/duck_coop/duck_lost_feathers.jpg)
<div class="image-caption">Their feathers are damp here, but you can see their down is being replaced by adult feathers.</div>

Fortunately they arrived today, 6 days before the projected delivery date. Alicia was busy with work, so my sister and I spent a few hours putting it together in our garage.

![Duckling-Feathers](/images/posts/projects/duck_coop/duck_house1.jpg)
<div class="image-caption">The walls went up first.</div>

It was pretty easy construction, but some of the parts were damaged in shipping so we had to drill some new holes and put in replacement screws.

![Duckling-Feathers](/images/posts/projects/duck_coop/duck_house2.jpg)
<div class="image-caption">All built!</div>

Once it was all put together, we decided that we needed to add some new latches that raccoons wouldn't be able to open. Also, although the lid is pretty heavy, we wanted to make sure it was latched so that wind wouldn't blow the top open. We may still need to add another latch later.

![Duckling-Feathers](/images/posts/projects/duck_coop/duck_house3.jpg)
<div class="image-caption">One of our extra latches.</div>

![Duckling-Feathers](/images/posts/projects/duck_coop/duck_house4.jpg)
<div class="image-caption">Settled into their new home.</div>


Knowing how fast these ducks are growing, they will be mostly full grown in just a few weeks. We hope that by then the weather will be warmer and we will be able to put their new home outside without the heat lamp. Then, during the day they'll be in their duck run and at night we will lock them up safely in their house so predators can't attack them. It will also make it easier to gather their eggs when they start laying (hopefully) in a few months.


<h4 class="underline--magical"> Day 12 --- Duck Runner </h4>

I was out of town, but my wife and sister completed our outside duck runner by finishing the chicken wire and spray painting the entire frame gray so that it was a nice uniform color. Fully completed, with a swinging door, it is a nice place for us to put the ducks so they can forage and spend time outside while the weather is nice. 

![Duckling-Runner](/images/posts/projects/duck_coop/runner.jpg)
<div class="image-caption">The finished duck runner, in a nice grey.</div>

On this day they also found that the ducklings love to hang out in the runner, but if the humans get too far away they come running! That was when we started latching the door shut when no one would be around. The whole thing is just to protect them from predators during the day, after all.

![Duckling-Run](/images/posts/projects/duck_coop/duck_first_yard.gif)
<div class="image-caption">They love to waddle.</div>


<h4 class="underline--magical"> Day 13 --- When Ducklings Attack </h4>


<youtube-embed video-id="BsAfvtCeYZY"></youtube-embed>

On one of the duckling's forage days, Sydney found that they really enjoyed pecking at her phone. Just for fun, she set the video to record and let them attack it to their hearts content. 


<h4 class="underline--magical"> Day 18 --- First Quack </h4>

I don't have anything to show for this day because, well, it's not visible. As the ducks have gotten bigger, they've still maintained their cute chirpy sounds that they make. That is, until today. Their chirps have slowly gotten deeper, and, much like a teenager with voice cracks, a small quack occasionally escapes them. They're still mostly chirping, but their small duckling voices are definitely changing.


<h4 class="underline--magical"> Day 22 --- Outdoor Swim </h4>


Living in a rural area on an orchard happens to have the perk that we have a small creek that runs in front of my parent's house. Part of this is perfect for our ducklings to swim around and forage in the water, since it's easy for us to watch them and they enjoy the water. They aren't quite yet fully waterproof, but they still seem to love the time in the water swimming, digging in the mud, and diving under the water.

![Duckling-Swim1](/images/posts/projects/duck_coop/duck_swim_1.jpg)


![Duckling-Swim-Gif](/images/posts/projects/duck_coop/duck_swim_outside.gif)
<div class="image-caption">Like an outdoor bath.</div>


<h4 class="underline--magical"> Day 23 --- Worms Be Gone! </h4>

Yesterday we began noticing that Tuk-tuk was having some trouble walking--he kept falling over and would sit back on his hocks (bird knees) when he was standing. It was also becoming noticeable that he wasn't growing as much as the other two ducks, and had become visibly smaller. After much Internet searching, I found that there's a lot of different conditions in poultry that have the symptom of lameness and weight loss. 

It seemed that the mostly likely candidates were either some sort of nutrient deficiency or some sort of parasitic worm. We have been supplementing their diet with brewer's yeast which has a large assortment of vitamins in it, including niacin (crucial to duck health). With that in mind, according to the Internet it's common to de-worm poultry a couple times per year anyway, so we decided to try that and see if our poor Tuk-tuk would improve. Fenbendazole is a common broad-spectrum de-wormer that's often used for poultry, and small pets such as cats and dogs. We picked some up, and then I did some math to figure out the dosage based on 30mg/kg for ducks. The recommended dose that I found online is really 20mg/kg, but I figured they would lose some in the grass as they ate it in their food.

![De-wormer](/images/posts/projects/duck_coop/dewormer.jpg)
<div class="image-caption">Safeguard canine de-wormer. Recommended 50mg/kg for dogs.</div>