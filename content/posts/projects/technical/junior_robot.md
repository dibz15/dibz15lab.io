---
post_title: A Self-Charging Autonomous Robot
hero: /images/posts/projects/junior_good_low.jpg
tags: 
  - project
date: 2019-12-20

project_title: Self-Charging Autonomous Robot
project_start: 2017-10-01
project_end: 2018-03-25
project_desc: An autonomous self-charging robot, which featured computer vision, IR communication, a Raspberry Pi, and text-to-speech. I led a three-man team to create this as part of a 14-week project at OSU. We achieved the highest technical award for our completed project.
unlisted_post: false
project_link: true

id: "junior_robot_overview"
---

I plan on writing a more extensive article/description of this project in the future, but 
for now I will just embed my team's video that gives a thorough overview of the project.

As a quick note, the major technologies used were Python3.6, OpenCV, and IR communication.

<youtube-embed video-id="kdMCKDFRIDs"></youtube-embed>

Also, although it's not really documented, the code that was used for this project can be found <tab-link link="https://github.com/Dibz15/fancy-robot">here</tab-link>.