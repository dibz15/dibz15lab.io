---
post_title: Pong
unlisted_post: true
hero: /images/posts/projects/pong_ai_play.gif
tags: 
  - project
date: 2020-01-08
project_title: Multiplayer Pong Using an STM32F4 and a LED Matrix Display
project_start: 2019-09-05
project_end: 2021-07-23
project_desc: Multiplayer pong table game made using an STM32F4-Nucleo board and a LED matrix display. I am writing the game engine from scratch in C. It has simple controls and a basic AI, as well as different modes to play. I also designed and laser-cut a nice acrylic case for it.
project_link: false

id: "stm32_pong_overview"
---