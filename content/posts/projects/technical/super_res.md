---
post_title: Super-Resolution
unlisted_post: true
hero: /images/posts/projects/super_res/netCompareBlur.png
tags: 
  - project
date: 2020-01-08

project_title: Super-Resolution using CNN's in PyTorch
project_start: 2018-11-20
project_end: 2018-12-05
project_desc: An independent final project for my 'digital image processing' class at OSU. I implemented and trained a custom CNN in PyTorch to achieve scaling (super-resolution) on input images.
project_link: true

id: "super_res_overview"
---


This project was extremely interesting, and I am finding new ways to use what I learned while I was working through it. PyTorch and Python are a great toolchain and allow for really interesting results. <tab-link link="/misc/posts/projects/super_res/report.pdf">Here</tab-link> is my final report for the project.