---
unlisted_post: false
hero: /images/posts/projects/face_detection_fasterrcnn/group_detection_hero.jpg
tags: 
  - project

project_title: Facial Detection with FasterRCNN
project_start: 2020-05-20
project_end: 2022-01-21
project_desc: A project for preparing and training a FasterRCNN model on the WIDER dataset to detect faces. Achieved good accuracy and performance.
project_link: true

post_title: Training FasterRCNN to Detect Faces on the WIDER dataset.
excerpt_desc: A machine learning project with PyTorch using transfer learning.
date: 2022-01-21
type: technical

id: "face_detection_with_fasterrcnn"
no_comments: false
---

[Hero image credit](https://www.pexels.com/photo/people-sitting-looking-at-a-laptop-7437491/)

<div class="table-of-contents">
Table of Contents.
  <ol>
    <li>
      <scroll-link link="#start">
        Gitlab Repository
      </scroll-link>
    </li>
    <li>
      <scroll-link link="#summary">
        Summary
      </scroll-link>
    </li>
    <li>
      <scroll-link link="#next-steps">
        Next Steps
      </scroll-link>
    </li>
  </ol>
</div>

<p id="start"/>

<h1 class="underline--magical"> Gitlab Repo </h1>

I've hosted my code for this project at [https://gitlab.com/dibz15/pytorch-fasterrcnn-trained-on-wider](https://gitlab.com/dibz15/pytorch-fasterrcnn-trained-on-wider).

<p id="summary"/>

<h1 class="underline--magical"> Summary </h1>

This project was my first independent foray into deep learning with PyTorch. I have worked with PyTorch before on my [super-resolution project](/posts/projects/technical/super_res), but I wanted to try something more challenging and (at the time I started this) state-of-the-art. 

My model is a modified FasterRCNN using the MobileNetV2 classifier network as the backbone (with frozen gradients). It is trained on the publicly available WIDER dataset (for which I wrote my own PyTorch dataloader). I trained my model for only 65 epochs on my RTX2060. The final model is about 300MB, and for an IoU threshold of 0.6, gives an mAP of about 0.5.

Here's a graph of mAP vs. IoU for my trained model:

![mAP vs. IoU graph](/images/posts/projects/face_detection_fasterrcnn/iou_thresh.jpeg)

This doesn't seem very good, but many of the examples in WIDER are very challenging. For the more typical example (and more like my own use-cases) the hero image of this article is a good example. It performs well! Also, check out my model's result on some animated examples:

![Video test 1, Conan](/images/posts/projects/face_detection_fasterrcnn/test_output_conan.gif)

![Video test 2, Thor](/images/posts/projects/face_detection_fasterrcnn/test_output_thor.gif)

I'm really happy with it. If you'd like to learn more about the model or about my source, check out my gitlab repository from above. I have tried to document it well, but if you have any questions feel free to contact me.

<p id="next-steps"/>

<h1 class="underline--magical"> Next Steps </h1>

Now that I have a working face detection model, I would ideally like to feed its output to a facial recognition model. If I can find the right type of recognition model, my hope is that I can train it recognize custom faces from my own dataset (like my own face) and have a highly accurate facial recognition tool. If I do end up starting such a project, I will link it here.