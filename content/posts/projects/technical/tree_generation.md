---
post_title: Procedurally Generating Trees with OpenGL and GLSL Shaders
excerpt_desc: A small class project featuring procedural trees and some cool shaders.
hero: /images/posts/projects/tree_grow.gif
tags: 
  - project
date: 2020-01-09

project_title: Procedurally Generated Trees with OpenGL and GLSL Shaders
project_start: 2019-11-28
project_end: 2019-12-06
project_desc: A solo class project for my computer graphics class at OSU. It featured a custom algorithm for procedurally generating trees. I also wrote some interesting fragment shaders to see how I could change the appearance of the trees.
unlisted_post: true
project_link: true

id: "tree_generation_overview"
---

In lieu of a longer write-up, which I hope to eventually write, I will embed some YouTube videos that show off my tree generation algorithm and shaders.

<youtube-embed video-id="MlAZ56NkGIw"></youtube-embed>

<youtube-embed video-id="YKnPXU8unbc"></youtube-embed>