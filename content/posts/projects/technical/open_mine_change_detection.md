---
unlisted_post: false
hero: /images/posts/projects/omcd/hero.png
tags: 
  - project
  - Machine Learning
  - Deep Learning
  - Change Detection

project_title: "Master's Dissertation: Characterising Open Cast Mining from Satellite Data"
project_start: 2023-05-01
project_end: 2023-08-14
project_desc: My master's dissertation project at the University of Strathclyde in Glasgow, UK.
project_link: true

post_title: Characterising Open Cast Mining from Satellite Data
excerpt_desc: A look at my master's dissertation project completed at the University of Strathclyde in Glasgow, UK.
date: 2023-08-21
type: technical

id: "masters_open_cast_mining"
no_comments: false
---

<div class="table-of-contents">
Table of Contents.
  <ol>
    <li>
      <scroll-link link="#summary">
        Summary
      </scroll-link>
    </li>
    <li>
      <scroll-link link="#abstract">
        Abstract
      </scroll-link>
    </li>
    <li>
      <scroll-link link="#fulltext">
        Full Text
      </scroll-link>
    </li>
    <li>
      <scroll-link link="#start">
        Gitlab Repository
      </scroll-link>
    </li>
  </ol>
</div>



<p id="summary"/>

<h1 class="underline--magical"> Summary </h1>

I'm excited to announce that I've officially submitted my master's dissertation as part of my MSc in Advanced Computer Science with Artificial Intelligence at the University of Strathclyde in Glasgow, UK. Under the supervision of Dr. Marc Roper, I delved into an subject entirely new to me: "Characterising Open Cast Mining from Satellite Data."

At the outset of the project, my knowledge in remote sensing and satellite imagery processing was almost zero. However, over the past 3 months, I have worked hard to complete my project and make a solid contribution to this research domain. I'm really pleased with how it turned out, so I wanted to share my work here on my site so that others may be able to learn from it. 

For those interested in the details, I have linked my source code repository <scroll-link link="#start">just below</scroll-link>. Please note that I've released my code under [OSL 3.0](https://opensource.org/license/osl-3-0-php/), and my dissertation and dataset under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0).

Overall, my work approaches surface mine activity assessment from the perspective of automation through [deep learning for change detection](https://ieeexplore.ieee.org/document/9136674). I present a novel Sentinel-2 dataset for change detection over surface mine areas, which I meticulously hand-labelled with ground-truth masks. Additionally, I present an index for activity detection in remote sensing images using change detection algorithms with optical satellite data. It's designed as an optical counterpart to the NDAI, a concept originated by [(Moon and Lee, 2021)](https://www.proquest.com/docview/2530134248/abstract/4D12507C8F4143PQ/1). 

While I was limited by the deadline and by hardware resource limitations, I am hopeful that my work can help to expand surface mine monitoring for robust environmental governance. There is a serious shortage of public datasets in this area, so I think that my public dataset will be an important contribution to the field.

<p id="abstract"/>
<h1 class="underline--magical"> Abstract </h1>

Here's the abstract from my dissertation:

"Open-pit mining, a prevalent surface mining method, is responsible for extracting a significant portion of the world’s minerals due to its economic advantages. However, this
technique poses considerable environmental and social challenges. The need for effective mapping and monitoring of global mining areas is becoming increasingly critical to
assess the associated environmental and anthropological risks. This research leverages remote sensing and deep learning technologies to monitor and detect spatio-temporal changes in open-pit mining regions. A comprehensive review of the literature is undertaken, examining recent studies on the identification of mining regions, the assessment of environmental impacts, the application of deep learning in change detection, and the surveillance of activity in surface mining areas. The research methodology details the creation of a unique dataset, OMS2CD, comprised of Sentinel-2
satellite images of mining regions which have been annotated for change detection. Deep
learning models, including TinyCD, LSNet, and DDPM-CD, are trained on this dataset. To
quantify the levels of activity, a novel metric, the Normalized Difference Temporal Change Index (NDTCI), is introduced. The performance of the models, the influence of cloud cover and seasonal variations, and the impact of utilizing masks for areas of interest is discussed. The application of the trained models in detecting changes and mapping activity levels in unseen mining sites is demonstrated through case studies.
The key findings include the strong correlation between the median model prediction and
ground truth data, and the utility of the NDTCI as a normalized measure of activity. In
essence, this research proposes a methodology that utilizes freely accessible data and
deep learning models to monitor surface mining regions by detecting changes in satellite
imagery, serving as a proxy for activity. The NDTCI offers a quantitative approach to
mapping the activity levels of mining sites over time."


<p id="fulltext"/>
<h1 class="underline--magical"> Full Text </h1>

Here's a link to the <tab-link link="/misc/posts/projects/omcd/dissertation.pdf">full text PDF</tab-link>.


<p id="start"/>

<h1 class="underline--magical"> GitHub Repository </h1>

I've hosted my code for my dissertation project at [https://github.com/Dibz15/OpenMineChangeDetection](https://github.com/Dibz15/OpenMineChangeDetection).




