---
post_title: Audio Visualizer in Processing
unlisted_post: true
hero: /images/posts/projects/audio_vis/audio_vis_anim.gif
tags: 
  - project
date: 2020-01-08

project_title: Live Audio/Music Visualizer in Processing (Java)
project_start: 2018-04-14
project_end: 2018-04-15
project_desc: An audio-visualizer that I wrote in Processing with my wife during an HP-sponsored HWeekend event at OSU. We achieved 2nd place and won some HP backpacks and an HP Sprocket printer.
project_link: true

id: "audio_vis_overview"
---

Longer write-up coming soon. For now, I'll link the project's git repo [here](https://github.com/Dibz15/audioVisual). 

This software was originally intended to be used with my [drawing robot project](/posts/projects/technical/draw_bot), and I was successful in getting it to output Gcode to my pen plotter in order to draw some cool visual patterns for each song, such as the pictures below.

![Banana Pancakes](/images/posts/projects/audio_vis/ban_pan.jpg)

![Danse Macabre](/images/posts/projects/audio_vis/dan_mac.jpg)

The next two images were taken at the HWeekend event.

![](/images/posts/projects/audio_vis/hweek1.jpg)

![](/images/posts/projects/audio_vis/hweek2.jpg)
