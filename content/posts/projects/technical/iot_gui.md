---
post_title: IoT GUI
unlisted_post: true
hero: /images/posts/projects/iot_gui.png
tags: 
  - project
date: 2020-01-10

project_title: Desktop GUI Built with Electron and Vue
project_start: 2018-03-20
project_end: 2018-05-01
project_desc: A custom desktop GUI application using Electron and Vue that was used to communicate via UDP with our IoT device. It featured multi-client communication, live setpoint, and graphing capabilities.
project_link: false

id: "iot_gui_overview"
---