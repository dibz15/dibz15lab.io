---
post_title: My Senior Capstone - Designing a New IIoT Device Ecosystem
hero: /images/posts/projects/custom_iot_low2.jpeg
excerpt_desc: A client-sponsored team design project that I undertook for my senior capstone at Oregon State University, with the goal of developing a new 'Universal' Industrial IoT device.
tags: 
  - project
date: 2020-01-07

project_title: Universal IIoT - My Senior Capstone Project
project_start: 2018-09-01
project_end: 2019-05-01
unlisted_post: true
project_link: true

id: "senior_iot_overview"
---

In the future I may have a thorough write-up with more information on this amazing project that I had the opportunity to work on. Until then, some information about the project is available on our official [project site](https://sites.google.com/a/oregonstate.edu/ece44x201812/ece-senior-design-example-project).


Below is a stylish picture of the actual device, of which I am very proud. For any serious business inquiries, the sponsor of the project was [IND LLC](https://indhq.com/), based in Vancouver, Washington. 

![](/images/posts/projects/senior_iot/ipb.jpg)


Below are a few memorable moments from the project at Oregon State University's Senior Design Expo 2019.


![](/images/posts/projects/senior_iot/expo_booth.jpg)

![](/images/posts/projects/senior_iot/expo_booth_mike.JPG)

![](/images/posts/projects/senior_iot/expo_booth_me.JPG)