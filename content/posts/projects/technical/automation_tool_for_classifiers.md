---
unlisted_post: false
hero: /images/posts/projects/classification_automation_tool/hero.gif
tags: 
  - project
  - Machine Learning

project_title: Automation Tool for Classification Datasets
project_start: 2020-05-20
project_end: 2022-03-09
project_desc: A project to assist the creation of classification datasets -- particularly for facial recognition.
project_link: true

post_title: Automating the Creation of Classification Datasets 
excerpt_desc: A machine learning project using PyTorch and OpenCV to automate the creation of image datasets.
date: 2022-03-09
type: technical

id: "automation_tool_for_face_recognition"
no_comments: false
---

<div class="table-of-contents">
Table of Contents.
  <ol>
    <li>
      <scroll-link link="#start">
        Gitlab Repository
      </scroll-link>
    </li>
    <li>
      <scroll-link link="#summary">
        Summary
      </scroll-link>
    </li>
  </ol>
</div>


<p id="start"/>

<h1 class="underline--magical"> Gitlab Repo </h1>

I've hosted my code for this project at [https://gitlab.com/dibz15/ml-classification-dataset-builder](https://gitlab.com/dibz15/ml-classification-dataset-builder).

<p id="summary"/>

<h1 class="underline--magical"> Summary </h1>

This tool/project is a continuation of my last deep learning project -- [Facial Detection with FasterRCNN](/posts/projects/technical/face_detection_with_fasterrcnn). I mentioned in that post that some next steps would be to move on facial recognition/classification, so this tool is an extension of that. I realized that to train a classifier on custom faces I would need a somewhat large dataset for each class of faces (each person), and so this project is an effort to help automate that process. Under the hood, it takes advantage of the trained facial detection model and uses it to crop target faces from media (images or videos) and then save those into a custom dataset.

With this tool, you can go from a video of a person like this:

![Obi-wan, annotated](/images/posts/projects/classification_automation_tool/obi-wan_annotated.gif)

and get a dataset output that looks like this:  

![Obi-wan, output classified](/images/posts/projects/classification_automation_tool/obi-wan_output.gif)

Here's an example of it in use classifying some members of the Shire:

![Hobbits example](/images/posts/projects/classification_automation_tool/hobbits.gif)

For more information on how to use it to make your own classification datasets, see my Gitlab page above. I go into a lot more depth about how to use it!
