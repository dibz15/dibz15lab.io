---
post_title: DIY Arduino Pen Plotter
hero: /images/posts/projects/draw_bot_hello.gif
tags: 
  - project
date: 2020-01-08
project_title: DIY Arduino Pen Plotter
project_start: 2017-12-27
project_end: 2018-04-14
project_desc: An Arduino-based pen plotter that I made using custom 3D-printed parts and a hot-glue gun. The final product ran smoothly, and could be used with any software that could output Gcode.
unlisted_post: true
project_link: false

id: "draw_bot_overview"
---