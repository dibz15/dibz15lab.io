---
post_title: Global Sea Ice Visualization
unlisted_post: true
excerpt_desc: Visualizing Global Sea Ice Area using Processing.
hero: /images/posts/projects/sea_ice_vis.gif
tags: 
  - project
date: 2020-01-09

project_title: Global Sea Ice 3D Visualization
project_start: 2019-12-02
project_end: 2019-12-04
project_desc: 3D Visualization of global sea ice area change over time. Created using Processing, and used as part of a larger research project.
project_link: true

id: "sea_ice_visual_overview"
no_comments: true
---

Below is the visualization as it was rendered. This was part of a larger individual class research project covering the effects of livestock on the global environment, which you can view <tab-link link="https://www.arcgis.com/apps/Cascade/index.html?appid=4f921fe5246b428aa260bb921b71099b">here</tab-link> if you like.

<youtube-embed video-id="nCtrG0N0wHM"></youtube-embed>
