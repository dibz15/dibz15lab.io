---
unlisted_post: true
hero: /images/posts/projects/bow_machine/bow_machine_electronics.jpg
tags: 
 - project
date: 2020-01-07

project_title: Product Test Automation Controller
project_desc: Shortly before my freshman year of college, I was hired by an archery company to create the electronic controller for automating the test process for recurve bows. During the course of the project I designed my first permanent circuit and PCB.
project_start: 2015-09-15
project_end: 2015-12-12
project_link: true

id: "bow_machine_overview"
---

After designing the circuit and writing the firmware on an Arduino, I tested the whole system on a single breadboard.

![](/images/posts/projects/bow_machine/IMG_2314.jpg)

<p class="pt-4 pb-2 blocktext">
  The company needed a working prototype ASAP so while I designed the PCB that would be the permanent solution, I created a temporary system using a protoboard.
</p>

![](/images/posts/projects/bow_machine/IMG_2173.jpg)


<p class="pt-4 pb-2 blocktext">
Below is my finished PCB, which I had manufactured by OSHPark.
</p>

![](/images/posts/projects/bow_machine/IMG_2348.jpg)

<p class="pt-4 pb-2">
I then populated the board with parts from digikey.
</p>

![](/images/posts/projects/bow_machine/IMG_2355.jpg)


<p class="pt-4 pb-2">
And I tested the whole system again using the PCB and a breadboard.
</p>

![](/images/posts/projects/bow_machine/IMG_2357.jpg)


<p class="pt-4 pb-2">
Finally I installed the system and ran the wiring.
</p>

![](/images/posts/projects/bow_machine/IMG_2359.jpg)

<p class="pt-4 pb-2">
Here's the system installed in the control box. Mounted on the box's front panel were some control buttons and LED's that were used to control the system.
</p>

![](/images/posts/projects/bow_machine/bow_machine_electronics.jpg)

<p class="pt-4 pb-2">
Finally, here's the system in action. Pretty simple, but it was a good experience.
</p>

![](/images/posts/projects/bow_machine/bow_machine.gif)