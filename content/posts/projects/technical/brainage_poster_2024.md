---
unlisted_post: false
hero: /images/posts/projects/brainage_poster_2024/hero.jpg
tags: 
  - project
  - Machine Learning
  - Deep Learning
  - Brain Age
  - neuroscience

project_title: "Poster: A Novel Foundation Model for Estimating Brain MRI Health"
project_start: 2024-05-31
project_end: 2024-06-11
project_desc: I presented my first conference poster at 2024 SINAPSE ASM, Celebrating Scottish Research Conference and University of Glasgow CCNi's 15th Anniversary conference. 
project_link: true

post_title: Presenting the First Conference Poster of my PhD
excerpt_desc: I presented my first conference poster at 2024 SINAPSE ASM, Celebrating Scottish Research Conference and University of Glasgow CCNi's 15th Anniversary conference. 
date: 2024-06-14
type: technical

id: "brainage_poster_2024"
no_comments: false
---

<p id="summary"/>
<h1 class="underline--magical"> Summary </h1>

Last week I had the opportunity to present the first poster of my PhD at the 2024 [SINAPSE](https://www.sinapse.ac.uk/) Annual Scientific Meeting, as well at the Celebrating Scottish Research Conference (co-hosted by the [Scottish Dementia Research Consortium](https://www.sdrc.scot/) and [Brain Health ARC](https://www.brainhealtharc.com/)), and at the University of Glasgow Centre for Cognitive Neuroscience ([CCNi](https://www.gla.ac.uk/schools/psychologyneuroscience/research/ccni/)) 15th Anniversary event. It was a busy 10 days!

![](/images/posts/projects/brainage_poster_2024/combined_logo.jpg)

It was really fun and interesting to see an overview of the neuroscience research that is being conducted across the UK and Europe. Via the events, I had the opportunity to meet a number of scientists and fellow PhD students and discuss our work. I presented my poster as a summary of my year's work, as well as with some ideas of what I will do next. At the Celebrating Scottish Research Conference, my colleague Connor Dalby (who is also supervised by my PI Dr. Michele Svanera) was awarded the Early Career Research prize for his presentation of his thesis project on cortical thickness estimation.

<ImageCarousel :images="[
    '/images/posts/projects/brainage_poster_2024/ccni_us.jpg',
    '/images/posts/projects/brainage_poster_2024/ccni_me_low.jpeg',
    '/images/posts/projects/brainage_poster_2024/ccni_me_2.jpg',
    '/images/posts/projects/brainage_poster_2024/ccni_everyone.jpeg',
    '/images/posts/projects/brainage_poster_2024/sdrc_me_low.jpg'
]"></ImageCarousel>

<!-- <h1 class="underline--magical"> Poster PDF </h1> -->

<!-- Here's my [poster link](https://drive.google.com/file/d/1NRXUbgMCIvDjBRfZpssAVHs5J7pbETkq/view?usp=sharing) (or see below).

<embed src="https://drive.google.com/viewerng/
viewer?embedded=true&url=https://drive.usercontent.google.com/download?id=11vm16PkDzz2XK2gR8LUwQzyMNz4uUnAM&export=download&authuser=0" width="75%" max-height=2000 height=1000> -->


I am currently working towards a publication of my work, so after our manuscript is published I will upload my poster here for others to see!
