
### Background

Although I was born and raised in a small town in Eastern Oregon, I have a deep passion for global languages and cultures.  I am married to a Colombian-American who helps keep me fluent in my Spanish, and I'm currently teaching myself how to speak Korean as well.

Being raised on a farm in a rural community has given me a strong work ethic, but I also enjoy pursuing hobbies in my free time. Currently I appreciate reading, graphite pencil art, programming, playing guitar and piano, and spending time with my family.