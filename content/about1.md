
###  Experience

I am a lifelong learner who has been mostly self-taught, and enjoys diving into just about any topic. I have had over four years of industry experience in software programming, embedded systems, and firmware design. My recent interests have taken me to the cross-roads between AI and neuroscience.

Throughout my studies and in my own spare time, I have learned and worked with UI design, machine learning, computer vision, web design, and more. I graduated Summa Cum Laude from Oregon State University in March of 2020 with a Electrical & Computer Engineering degree and a Spanish minor. Since then, I have worked as a software engineer for Siemens Digital Industries Software and as an engineering contractor. Along the way, I found that I have a passion for artificial intelligence and I completed my master's degree in computer science at the University of Strathclyde in Glasgow, Scotland. Starting in 2023, I am now studying my PhD in neuroscience at the University of Glasgow! (see me in the kilt, left)


