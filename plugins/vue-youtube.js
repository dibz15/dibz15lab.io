import Vue from 'vue';
import Youtube from 'vue-youtube';
import getIdFromUrl from 'get-youtube-id'

Vue.use(Youtube);
Vue.use(getIdFromUrl);