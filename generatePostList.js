const fs = require("fs");
var path = require("path");

const getAllFiles = function(dirPath, arrayOfFiles) {
  files = fs.readdirSync(dirPath)
 
  arrayOfFiles = arrayOfFiles || []
 
  files.forEach(function(file) {
    if (fs.statSync(dirPath + "/" + file).isDirectory()) {
      arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
    } else {
      arrayOfFiles.push(path.join(dirPath, "/", file))
    }
  })
 
  return arrayOfFiles
} 

const generatePostList = function (base = 'static') {
  const arr = getAllFiles(path.join('.', base, 'posts'), null);

  var fileData = { posts: [] };

  for (var i = 0, len = arr.length; i < len; i++) {
    let file = arr[i];
    const data = fs.readFileSync(`${file}`);
    file = file.replace(base, '');
    file = file.substr(0, file.lastIndexOf('.'));
    fileData.posts.push( { 'url': `${file}/`, 'filePath': `~${base}${file}.md` });
  }
  // console.log(fileData);
  var fileContent = JSON.stringify(fileData);
  fs.writeFileSync(path.join('.', base, 'posts.js'), fileContent);
  return fileData;
}

// var directoryPath = path.join(__dirname, '/static/posts');
module.exports = {
  // generatePostList: function (callback) {
  //   const arr = getAllFiles('./static/posts', null);

  //   var fileData = { posts: [] };

  //   var itemsProcessed = 0;

  //   arr.forEach( (file) => {
  //     fs.readFile(`${file}`, data => {
  //       const { url } = fm(data);
  //       file = file.replace('static', '');
  //       file = file.substr(0, file.lastIndexOf('.'));

  //       fileData.posts.push( { 'url': `${file}`, 'filePath': `~static${file}.md` });
  //       console.log(fileData);
  //       itemsProcessed++;
  //       if (itemsProcessed === arr.length) {
  //         const f = function () {
  //           var fileContent = JSON.stringify(fileData);
  //           fs.writeFile('./static/posts.js', fileContent, (err) => {
  //             if (err) {
  //               console.err(err);
  //             }
  //             callback(fileData, err);
  //           });
  //         }
  //         f();
  //       }
  //     });
  //   } );
  // }, 
  generatePostList
}

const fileData = generatePostList('content');
console.log(`Post URL Data:`);
console.log(fileData);