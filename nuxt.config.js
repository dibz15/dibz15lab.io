const path = require('path')

const colors = require('vuetify/es5/util/colors').default
const generatePostList = require('./generatePostList').generatePostList

const { CI_PAGES_URL } = process.env

const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

const domain = 'https://austindibble.com'
const description = 'A lifelong learner, engineer, and developer. I pride myself in hard work and an interest in everything.'

var urls = null;

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
  ? '/public/_nuxt/'
  : '/',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - Austin Dibble',
    // title: 'Austin Dibble' || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: description || '' },
      { property: 'og:type', content: 'website' },
      // {
      //   property: 'og:description',
      //   content: description
      // }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.css' }
      // { rel: "stylesheet", href: "~/assets/prism.css" }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  content: {
    liveEdit: false,
    markdown: {
      prism: {
        theme: 'prism-themes/themes/prism-dracula.css'
      },
      remarkPlugins: [
        'remark-math'
      ],
      rehypePlugins: [
             // this next line here
	      ['rehype-katex', { output: 'html' }]
      ]
    }
  },
  components: true,

    /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public',
    routes: function () {
      if (urls) {
        return urls;
      }
      console.log('Generating Post List...');
      const fileData = generatePostList('content');
      let posts = fileData.posts;
      if (!posts) 
        return '';
      console.log('Post Data: ');
      console.log(posts);
      urls = posts.map(data => data.url);
      console.log('Generated URLs: ');
      console.log(urls);
      return urls;
    }
  },

  render: {
    resourceHints: false
  },

  /*
  ** Customize the base url
  */
  router: {
    base
  },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/main.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vue-scroll-reveal', mode: 'client', ssr: false },
    { src: '~/plugins/vue-youtube', mode: 'client', ssr: false },
    { src: '~/plugins/disqus', mode: 'client', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    '@nuxt/content'
    // '@nuxtjs/markdownit'
  ],

  sitemap: {
    cacheTime: 1000 * 60 * 60 * 3,
    path: '/sitemap.xml',
    hostname: `${domain}/`,
    gzip: true,
    exclude: ['/404'],
    routes: () => {
      if (urls) {
        return urls;
      }
      console.log('Generating Sitemap..');
      const fileData = generatePostList('content');
      let posts = fileData.posts;
      if (!posts) 
        return '';
      urls = posts.map(data => data.url);
      console.log('Generated URLs: ');
      console.log(urls);
      return urls;
    }
  },
  robots: {
    sitemap: `${domain}/sitemap.xml`
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },

  /*
  *
  * Markdown-it module configuration
  */
  // markdownit: {
  //   // preset: 'default',
  //   linkify: true,
  //   // injected: true,
  //   // breaks: true,
  //   use: [
  //     // 'markdown-it-div',
  //     // 'markdown-it-attrs'
  //   ]
  // },

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          background: '#151515',
          base: colors.grey.darken4,
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    // vendor: ['prismjs'],
    /*
    ** You can extend webpack config here
    */
    extend (config, { isClient }) {
      config.module.rules.push(
        {
          test: /\.md$/,
          include: path.resolve(__dirname, 'content'),
          loader: 'frontmatter-markdown-loader',
          options: {
            mode: ["html", "vue-component"]
          }
        }
      )

      if (isClient) {
        config.devtool = 'source-map'
      }
    }
  },
  target: 'static',
  ssr: true
}
